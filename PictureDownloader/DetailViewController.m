//
//  DetailViewController.m
//  PictureDownloader
//
//  Created by amttgroup on 15-5-21.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "DetailViewController.h"
#import "Picture.h"
@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,0), ^{
        CGFloat scale = [UIScreen mainScreen].scale;
        UIImage * image = [UIImage imageWithData:self.picture.imageData];
        CGSize size;
        float widthHeightRadio = image.size.width/image.size.height;
        if (image.size.width > image.size.height) {
            float width = image.size.width > self.view.frame.size.width ? MAX(image.size.width,self.view.frame.size.width) : MIN(image.size.width,self.view.frame.size.width);
            size = CGSizeMake(width, width/widthHeightRadio);
        } else {
            float height = image.size.height > self.view.frame.size.height ? MAX(image.size.height, self.view.frame.size.height) : MIN(image.size.height, self.view.frame.size.height);
            size = CGSizeMake(widthHeightRadio*height, height);
        }
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(size.width, size.height), YES, scale);
        [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
        UIImage * newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImageView * imageView = [[UIImageView alloc] initWithImage:newImage];
            self.scrollView.contentSize = imageView.frame.size;
            [self.scrollView addSubview:imageView];
            NSLog(@"%f %f %f %f",imageView.frame.origin.x,imageView.frame.origin.y,imageView.frame.size.width,imageView.frame.size.height);
        });
    });
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [self.view addGestureRecognizer:tap];
}

- (void) tap:(UITapGestureRecognizer*) tap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
