//
//  PictureCollectionViewController.m
//  PictureDownloader
//
//  Created by amttgroup on 15-5-20.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "PictureCollectionViewController.h"
#import "PicutureCell.h"
#import "DetailViewController.h"
@interface PictureCollectionViewController ()
- (IBAction)reset:(id)sender;
@end

@implementation PictureCollectionViewController

static NSString * const reuseIdentifier = @"picture";

- (void) viewWillAppear:(BOOL)animated
{
    [self.pictureCollection addObserver:self forKeyPath:@"count" options:0 context:NULL];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [self.pictureCollection removeObserver:self forKeyPath:@"count" context:NULL];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.pictureCollection.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PicutureCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    cell.picture = [self.pictureCollection pictureAtIndex:indexPath.row];
    return cell;
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"detail" sender:indexPath];
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
}

- (IBAction)reset:(id)sender
{
    [[self pictureCollection] reset];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    DetailViewController * detail = segue.destinationViewController;
    detail.picture = [self.pictureCollection pictureAtIndex:((NSIndexPath*)sender).row];
}


@end
