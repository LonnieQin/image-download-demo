//
//  Picture.h
//  PictureDownloader
//
//  Created by amttgroup on 15-5-20.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Picture : NSManagedObject

@property (nonatomic, retain) NSString * remoteURLString;
@property (nonatomic, retain) NSString * localURLString;
@property (nonatomic, retain) NSData * imageData;

@end
