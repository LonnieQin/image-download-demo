//
//  PictureCollection.m
//  PictureDownloader
//
//  Created by amttgroup on 15-5-20.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "PictureCollection.h"
#import "Picture.h"
#import "Picture+RemoteDownload.h"
#import "DownLoadManager.h"
@interface PictureCollection()
@property (nonatomic, readwrite, copy) NSURL * URL;
@property (nonatomic, readwrite, strong) DownLoadManager * downloadManager;
@property (nonatomic, readwrite, strong) NSMutableArray * pictures;
@end

NSString * const kDownloadManagerIdentifier = @"com.lonnie.picturedownloader";
@implementation PictureCollection

- (instancetype) initWithURL:(NSURL *)URL
{
    self = [super init];
    if (self) {
        _URL = URL;
        _downloadManager = [DownLoadManager downloadManagerWithIdentifier:kDownloadManagerIdentifier];
        [self updatePictures];
    }
    return self;
}

- (void) updatePictures
{
    NSURL * metadata = [NSURL URLWithString:@"http://allappswebserver.sinaapp.com/PictureDownloader/images.txt"];
    [self.downloadManager fetchDataAtURL:metadata completionHandler:^(NSData * data, NSError * error) {
        if (error) {
            NSLog(@"Error Downloading metadata:%@",error);
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self updatePicturesWithMetaData:data];
            });
        }
    }];
}

- (NSURL*) downloadURLForPath:(NSString*) path
{
    static NSURL * documentDirectoryURL;
    static dispatch_once_t  onceToken;
    dispatch_once(&onceToken, ^{
        documentDirectoryURL = [NSURL URLWithString:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]];
    });
    return [documentDirectoryURL URLByAppendingPathComponent:path];
}

- (void) updatePicturesWithMetaData:(NSData*) metadata
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    [self willChangeValueForKey:@"count"];
    NSError * error;
    NSString * jsonString = [[NSString alloc] initWithData:metadata encoding:NSUTF8StringEncoding];
    NSArray * pictureInfos = [NSJSONSerialization JSONObjectWithData:metadata options:0 error:&error];
    if (!pictureInfos) {
        return;
    }
    
    self.pictures = [NSMutableArray new];
    for (NSDictionary * pictureInfo in pictureInfos) {
        NSString * path = pictureInfo[@"path"];
        if (!path) {
            NSLog(@"Missing path:%@",jsonString);
        } else {
            NSURL * URL = [NSURL URLWithString:path];
            Picture * picture = [[Picture alloc] initWithRemoteURLString:URL.absoluteString downloadManager:self.downloadManager];
            NSLog(@"%s %@",__func__,URL.absoluteString);
            [self.pictures addObject:picture];
    
        }
    }
                              
    [self didChangeValueForKey:@"count"];
}

- (void) reset
{
    NSFileManager * fm = [NSFileManager defaultManager];
    for (Picture * p in self.pictures) {
        NSError * error;
        [fm removeItemAtURL:[NSURL URLWithString:p.localURLString] error:&error];
        NSLog(@"%@",error);
    }
}

- (Picture*) pictureAtIndex:(NSUInteger)index
{
    return [self.pictures objectAtIndex:index];
}

- (NSUInteger) count
{
    return self.pictures.count;
}

@end
