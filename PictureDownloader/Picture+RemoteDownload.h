//
//  Picture+RemoteDownload.h
//  PictureDownloader
//
//  Created by amttgroup on 15-5-20.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "Picture.h"
#import "DownLoadManager.h"
@interface Picture (RemoteDownload)
@property (nonatomic) DownLoadManager * downloadManager;
- (instancetype) initWithRemoteURLString:(NSString*) remoteURLString downloadManager:(DownLoadManager*) downloadManager;
@end
