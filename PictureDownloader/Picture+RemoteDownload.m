//
//  Picture+RemoteDownload.m
//  PictureDownloader
//
//  Created by amttgroup on 15-5-20.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "Picture+RemoteDownload.h"
#import "AppDelegate.h"
#import <objc/runtime.h>
@implementation Picture (RemoteDownload)
const char key;
- (void) setDownloadManager:(DownLoadManager *)downloadManager
{
    objc_setAssociatedObject(self, &key, downloadManager, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (DownLoadManager*) downloadManager
{
    return objc_getAssociatedObject(self, &key);
}


- (instancetype) initWithRemoteURLString:(NSString *)remoteURLString downloadManager:(DownLoadManager *)downloadManager
{
    self = [NSEntityDescription insertNewObjectForEntityForName:@"Picture" inManagedObjectContext:App.managedObjectContext];
    if (self) {
        self.downloadManager = downloadManager;
        self.remoteURLString = remoteURLString;
        if (![[NSFileManager defaultManager] fileExistsAtPath:[[downloadManager localURLForRemoteURL:[NSURL URLWithString:remoteURLString]] path]]) {
            [self.downloadManager downloadURLToDocuments:[NSURL URLWithString:remoteURLString]];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadManagerDidDownloadFile:) name:DownloadManagerDidDownladFileNotification object:nil];
        }
        [self reloadImage];
    }
    return self;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) downloadManagerDidDownloadFile:(NSNotification*) note
{
    NSURL * URL = [note userInfo][DownloadManagerSourceURLKey];
    if ([URL isEqual:[NSURL URLWithString:self.remoteURLString]]) {
        [self reloadImage];
    }
}

- (void) reloadImage
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData * data = [NSData dataWithContentsOfFile:self.localURLString];
        dispatch_async(dispatch_get_main_queue(),^{
            self.imageData = data;
        });
    });
}

- (NSString*) localURLString
{
    return [self.downloadManager localURLForRemoteURL:[NSURL URLWithString:self.remoteURLString]].path;
}


@end
