//
//  DownLoadManager.h
//  PictureDownloader
//
//  Created by amttgroup on 15-5-20.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <Foundation/Foundation.h>
extern NSString * const DownloadManagerDidDownladFileNotification;
extern NSString * const DownloadManagerSourceURLKey;
extern NSString * const DownloadManagerDestinationURLKey;

@protocol DownloadManagerDelegate;
@interface DownLoadManager : NSObject
@property (copy) void (^backgroundSessionCompletionHandler)();
+ (instancetype) downloadManagerWithIdentifier:(NSString*) identifier;
- (void) fetchDataAtURL:(NSURL*)URL
      completionHandler:(void (^)(NSData*,NSError *))completion;
- (void) downloadURLToDocuments:(NSURL*) sourceURL;
- (NSURL*) localURLForRemoteURL:(NSURL*) remoteURL;
@end

@protocol DownloadManagerDelegate <NSObject>
- (void) downloadmanager:(DownLoadManager*) manager
didFinishDownloadFromURL:(NSURL*) sourceURL
                   toURL:(NSURL*) destURL;

@end