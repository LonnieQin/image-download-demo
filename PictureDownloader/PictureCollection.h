//
//  PictureCollection.h
//  PictureDownloader
//
//  Created by amttgroup on 15-5-20.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Picture;
@interface PictureCollection : NSObject
@property (nonatomic,readonly) NSUInteger count;

- (instancetype) initWithURL:(NSURL*) URL;
- (Picture*) pictureAtIndex:(NSUInteger) index;
- (void) reset;
@end
