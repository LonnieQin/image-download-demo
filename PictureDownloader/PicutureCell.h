//
//  PicutureCell.h
//  PictureDownloader
//
//  Created by amttgroup on 15-5-20.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Picture;
@interface PicutureCell : UICollectionViewCell
@property (nonatomic,readwrite,strong) Picture * picture;
@end
