//
//  PictureCollectionViewController.h
//  PictureDownloader
//
//  Created by amttgroup on 15-5-20.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PictureCollection.h"
@interface PictureCollectionViewController : UICollectionViewController
@property (nonatomic,readwrite,strong) PictureCollection * pictureCollection;
@end
