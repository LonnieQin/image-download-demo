//
//  AppDelegate.h
//  PictureDownloader
//
//  Created by amttgroup on 15-5-20.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PictureCollection.h"
#import "PictureCollectionViewController.h"
#import "DownLoadManager.h"
#import <CoreData/CoreData.h>
#define App ((AppDelegate*)[[UIApplication sharedApplication] delegate])
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

