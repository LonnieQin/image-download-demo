//
//  DownLoadManager.m
//  PictureDownloader
//
//  Created by amttgroup on 15-5-20.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "DownLoadManager.h"
NSString * const DownloadManagerDidDownladFileNotification = @"DownloadManagerDidDownladFileNotification";
NSString * const DownloadManagerSourceURLKey = @"DownloadManagerSourceURLKey";
NSString * const DownloadManagerDestinationURLKey = @"DownloadManagerDestinationURLKey";
@interface DownLoadManager()<NSURLSessionDelegate,NSURLSessionDownloadDelegate>
@property (nonatomic,readwrite,strong) NSURLSession * foregroundSession;
@property (nonatomic,readwrite,strong) NSURLSession * backgroundSession;
@end
@implementation DownLoadManager

- (instancetype) init {
    NSAssert(NO,@"Use +downloadManagerWithIdentifier");
    return nil;
}

+ (instancetype) downloadManagerWithIdentifier:(NSString *)identifier
{
    static NSMutableDictionary * downloadManagers;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        downloadManagers = [NSMutableDictionary new];
    });
    DownLoadManager * downloadManager = downloadManagers[identifier];
    if (!downloadManager) {
        downloadManager = [[self alloc] initWithIdentifier:identifier];
        downloadManagers[identifier] = downloadManager;
    }
    return downloadManager;
}

- (instancetype) initWithIdentifier:(NSString*) identifier{
    self = [super init];
    if (self) {
        NSURLSessionConfiguration * configuration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:identifier];
        _backgroundSession = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil] ;
        _foregroundSession = [NSURLSession sharedSession];
    }
    return self;
}

- (void) fetchDataAtURL:(NSURL *)URL completionHandler:(void (^)(NSData *, NSError *))completion
{
    NSLog(@"%s:%@",__PRETTY_FUNCTION__,URL);
    NSURLSessionDataTask * task = [self.foregroundSession dataTaskWithURL:URL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        completion(data,error);
    }];
    [task resume];
    NSLog(@"exit:%s",__PRETTY_FUNCTION__);
}

- (void) downloadURLToDocuments:(NSURL *)sourceURL
{
    NSURLSessionDownloadTask * task = [self.backgroundSession downloadTaskWithURL:sourceURL];
    [task resume];
}

- (NSURL*) localURLForRemoteURL:(NSURL *)remoteURL
{
    static NSURL * documentDirectoryURL;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString  * docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
        documentDirectoryURL = [NSURL fileURLWithPath:docPath];
    });
    NSString * filename = [remoteURL lastPathComponent];
    return [documentDirectoryURL URLByAppendingPathComponent:filename];
}

- (void) moveFileFromLocation:(NSURL*) location forDownloadTask:(NSURLSessionDownloadTask*) downloadTask
{
    NSURL * sourceURL = [[downloadTask originalRequest] URL];
    NSURL * destURL = [self localURLForRemoteURL:sourceURL];
    
    NSError * error;
    NSFileManager * fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:[destURL path]]) {
        [fm removeItemAtURL:destURL error:NULL];
    }
    
    if ([[NSFileManager defaultManager] moveItemAtURL:location toURL:destURL error:&error]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:DownloadManagerDidDownladFileNotification
                                                            object:self userInfo:@{DownloadManagerSourceURLKey:sourceURL,DownloadManagerDestinationURLKey:destURL}];
    } else {
        NSLog(@"can not move file:%@",error);
    }
}


- (void) URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    NSLog(@"%s %@",__PRETTY_FUNCTION__,location);
    [self moveFileFromLocation:location forDownloadTask:downloadTask];
    
}

#pragma mark - URLSessionDelegate
- (void) URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    [self.backgroundSession getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        NSUInteger count = [dataTasks count] + [uploadTasks count] + [downloadTasks count];
        if (count == 0) {
            if (self.backgroundSessionCompletionHandler) {
                void (^completionHandler)() = self.backgroundSessionCompletionHandler;
                self.backgroundSessionCompletionHandler = nil;
                completionHandler();
            }
        }
    }];
}

@end
