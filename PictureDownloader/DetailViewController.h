//
//  DetailViewController.h
//  PictureDownloader
//
//  Created by amttgroup on 15-5-21.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Picture;
@interface DetailViewController : UIViewController
@property (nonatomic) Picture * picture;
@end
