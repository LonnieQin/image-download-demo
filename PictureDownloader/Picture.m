//
//  Picture.m
//  PictureDownloader
//
//  Created by amttgroup on 15-5-20.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "Picture.h"


@implementation Picture

@dynamic remoteURLString;
@dynamic localURLString;
@dynamic imageData;

@end
