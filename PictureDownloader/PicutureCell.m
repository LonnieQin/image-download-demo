//
//  PicutureCell.m
//  PictureDownloader
//
//  Created by amttgroup on 15-5-20.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "PicutureCell.h"
#import "Picture.h"
@interface PicutureCell()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
@implementation PicutureCell
- (void) setPicture:(Picture *)picture
{
    if (_picture) {
        [_picture removeObserver:self forKeyPath:@"imageData"];
    }
    
    _picture = picture;
    if (_picture) {
        [_picture addObserver:self forKeyPath:@"imageData" options:0 context:NULL];
    }
    [self reloadImageView];
}

- (void) reloadImageView
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,0), ^{
        CGFloat scale = [UIScreen mainScreen].scale;
        UIImage * image = [UIImage imageWithData:self.picture.imageData];
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(self.imageView.frame.size.width, self.imageView.frame.size.height), YES, scale);
        [image drawInRect:CGRectMake(0, 0, self.imageView.frame.size.width*scale, self.imageView.frame.size.height*scale)];
        __block UIImage * newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imageView.image = newImage;
        });
    });
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    [self reloadImageView];
}
@end
